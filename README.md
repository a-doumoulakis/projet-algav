# README #

### What is this repository for? ###

* Projet d'ALGAV 2015
* V0.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### A faire ###

- Les fonctions pour les tries hybrides
- La génération de graphes pour les arbres de la Briandais
- Les tests demandés dans le sujet
- Le rapport
- [Git avec tout qui est déjà fait](https://github.com/sbksba/ALGAV-4I500)
