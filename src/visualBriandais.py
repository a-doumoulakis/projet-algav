__author__ = 'lboutin'
import graphviz as gv

cpt = 0

def briandais_to_gviz(brd, file):
    g = gv.Graph(format='svg')
    g.attr('graph', {'bgcolor': 'honeydew', 'splines': 'ortho'})

    briandais_to_gviz_rec(brd, g)

    filename = g.render(filename='../gviz/' + file)
    print(filename)


def briandais_to_gviz_rec(brd, g, isFrere=False):
    global cpt
    if brd is None:
        return False
    else:
        node_id = str(brd.lettre) + str(cpt)
        cpt += 1

        node_label = str(brd.lettre)
        node_attr = {'label': node_label}
        if isFrere is True:
            node_attr['shape'] = 'diamond'
            node_attr['color'] = 'red'
        g.node(node_id, **node_attr)

        id_fils = briandais_to_gviz_rec(brd.fils, g, False)
        if brd.fils is not None and id_fils is not None:
            g.edge(node_id, str(id_fils), **{'xlabel': 'fils', 'color': ' forestgreen'})
        else:
            node_none = 'None'+str(cpt)
            g.node(node_none, **{'style': 'invis'})
            g.edge(node_id, node_none, **{'xlabel': 'fils', 'color': ' forestgreen'})
            cpt += 1

        id_frere = briandais_to_gviz_rec(brd.frere, g, True)
        if brd.frere is not None and id_frere is not None:
            g.edge(node_id, str(id_frere), **{'xlabel': 'frere', 'color': 'orangered'})
        else:
            node_none = 'None'+str(cpt)
            g.node(node_none, **{'style': 'invis'})
            g.edge(node_id, node_none, **{'xlabel': 'frere', 'color': 'orangered'})
            cpt += 1

        return node_id

def main():
    brd = Briandais('A')

    # brd.ajouter_mot("lou")
    # brd.ajouter_mot("leve")
    # brd.ajouter_mot("les")
    # brd.ajouter_mot("loups")
    # brd.ajouter_mot("dans")
    # brd.ajouter_mot("le")
    # brd.ajouter_mot("lourd")
    # brd.ajouter_mot("tapis")
    # brd.ajouter_mot("de")
    # brd.ajouter_mot("luxe")
    # brd.ajouter_mot("vert")
    # brd.ajouter_mot("olive")

    #brd.ajouter_mot("TACG")
    #brd.ajouter_mot("AAT")
    #brd.ajouter_mot("AT")
    #brd.ajouter_mot("CGGA")
    #brd.ajouter_mot("TAC")

    brd_exemple = Briandais.file_to_briandais('../exemple_base.txt')


    briandais_to_gviz(brd_exemple)
    liste = []
    print(brd.liste_mots(liste))
    print("GVIZ GENERATION DONE")

if __name__ == "__main__":
    main()
