import cProfile
import sys
from TrieHybride import TrieHybride
from Briandais import Briandais

if __name__ == "__main__":
    if "test" in sys.argv:
        f_test = 1
    else:
        f_test = 0
    print("Exemple de base : Trie Hybride")
    test = TrieHybride.file_to_trie("exemple_base.txt")
    liste = test.liste_mots()
    print("Liste des mots :", liste)
    print("Nombre de mots :", test.cpt_mot())
    print("Nombre de pointeur vers nil :", test.cpt_none())
    print("Hauteur de l'abre :", test.hauteur_trie())
    print("Profondeur moyenne :", test.profondeur_moyenne())
    print("prefixe 'dactylo' :", test.nombre_prefix("dactylo"))

    # test2 = Briandais("A")
    # test2 = TrieHybride.thybrid_to_briandais_pour_les_nuls(test, test2)
    # liste = []
    # liste = test2.liste_mots(liste)
    # print(liste)
    test = TrieHybride.dir_to_trie("./Shakespeare")
    if f_test is 1:
        print('\033[91m', "\n Construction Trie \n\n", '\033[0m')
        cProfile.run('TrieHybride.dir_to_trie("./Shakespeare")')

        print('\033[91m', "\n Ajout mot \n\n", '\033[0m')
        cProfile.run('test.ajout_mot("barbapapa")')

        print('\033[91m', "\n Liste mots \n\n", '\033[0m')
        cProfile.run('test.liste_mots()')

        print('\033[91m', "\n Compte mots \n\n", '\033[0m')
        cProfile.run('test.cpt_mot()')

    liste = test.liste_mots()
    liste.sort()
    file = open("word_list/hybride.txt", 'w')
    for el in liste:
        file.write(el + '\n')
    file.close()
    print("nombre de mots", test.cpt_mot())
    print("profondeur moyenne :", test.profondeur_moyenne())
    print("hauteur :", test.hauteur_trie())
    print("cpt none:", test.cpt_none())
    
    
