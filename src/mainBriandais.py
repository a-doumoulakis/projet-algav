from TrieHybride import *

if __name__ == "__main__":

    print('\033[91m', "\n Construction Arbre 'salut'\n", '\033[0m')
    test = Briandais("salut")
    test.best_print()
    print("est vide :", test.est_vide())
    
    print('\033[91m', "\n Ajout salutation \n", '\033[0m')
    test = test.ajouter_mot("salutation")
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("nombre mots :", test.nombre_mots())
    print("'salutation' est present :", test.est_present("salutation"))
    print("'salut' est present :", test.est_present("salut"))
    
    print('\033[91m', "\n ajout Salade \n", '\033[0m')
    test = test.ajouter_mot("salade")
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("nombre mots :", test.nombre_mots())
    print("'salade' est present :", test.est_present("salade"))
    
    print('\033[91m', "\n Ajout toto \n", '\033[0m')
    test = test.ajouter_mot("toto")
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("nombre mots :", test.nombre_mots())
    print("'toto' est present :", test.est_present("toto"))
    print("pointeurs vers vide :", test.cmp_none())
    print("hauteur :", test.hauteur_arbre())
    
    print('\033[91m', "\n suppression toto \n", '\033[0m')
    test = test.supprimer_mot("toto")
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("'toto' est present :", test.est_present("toto"))
    
    print('\033[91m', "\n suppression salut \n", '\033[0m')
    test = test.supprimer_mot("salut")
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("'salut' est present :", test.est_present("salut"))
    print("pointeurs vers vide :", test.cmp_none())
    print("hauteur :", test.hauteur_arbre())

    print('\033[91m', "\n fusion test et toto \n", '\033[0m')
    toto = Briandais("toto")
    test = Briandais.fusion_arbre(test, toto)
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    
    print('\033[91m', "\n fusion test et salut \n", '\033[0m')
    salut = Briandais("salut")
    test = Briandais.fusion_arbre(test, salut)
    test.best_print()
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("pointeurs vers vide :", test.cmp_none())
    print("hauteur :", test.hauteur_arbre())  
    print("profondeur moyenne :", test.profondeur_moyenne())
    print("nb prefix 'sal' :", test.nombre_prefix("sal"))
    print("nb prefix 'salut' :", test.nombre_prefix("salut"))
    test2 = TrieHybride()
    print('\033[91m', "\n Convertion arbre -> trie \n", '\033[0m')
    test2 = TrieHybride.briandais_to_hybride(test, None)
    liste = test2.liste_mots()
    print("liste :", liste)
