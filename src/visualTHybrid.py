__author__ = 'lboutin'
import graphviz as gv


cpt = 0
def thybrid_to_gviz(th, file):
    g = gv.Graph(format='svg')
    g.attr('graph', {'bgcolor': 'honeydew'})
    thybrid_to_gviz_rec(th, g)

    filename = g.render(filename='../gviz/' + file)
    print(filename)


def thybrid_to_gviz_rec(th, g):
    global cpt
    if th is None:
        return False
    else:
        node_id = str(th.val)+str(cpt)
        cpt += 1
        node_label = str(th.val)
        node_attr = {}
        if th.isWord is True:
            node_label += '*'
        node_attr['label'] = node_label
        g.node(node_id, **node_attr)

        id_inf = thybrid_to_gviz_rec(th.inf, g)
        if th.inf is not None and id_inf is not None:
            g.edge(node_id, str(id_inf), **{'label': 'inf', 'color': 'darkred', 'penwidth': '3'})
        else:
            node_none = 'None'+str(cpt)
            g.node(node_none, **{'style': 'invis'})
            g.edge(node_id, node_none, **{'label': 'inf', 'color': 'darkred', 'penwidth': '3'})
            cpt += 1

        id_eq = thybrid_to_gviz_rec(th.eq, g)
        if th.eq is not None and id_eq is not None:
            g.edge(node_id, id_eq, **{'label': 'eq', 'color': 'forestgreen', 'penwidth': '3'})
        else:
            node_none = 'None'+str(cpt)
            g.node(node_none, **{'style': 'invis'})
            g.edge(node_id, node_none, **{'label': 'eq', 'color': 'forestgreen', 'penwidth': '3'})
            cpt += 1
        id_sup = thybrid_to_gviz_rec(th.sup, g)

        if th.sup is not None and id_sup is not None:
            g.edge(node_id, id_sup, **{'label': 'sup', 'color': 'royalblue', 'penwidth': '3'})
        else:
            node_none = 'None'+str(cpt)
            g.node(node_none, **{'style': 'invis'})
            g.edge(node_id, node_none, **{'label': 'sup', 'color': 'royalblue', 'penwidth': '3'})
            cpt += 1


    return node_id


def main():
    th = TrieHybride()

    th.ajout_mot("lou")
    th.ajout_mot("leve")
    th.ajout_mot("les")
    th.ajout_mot("loups")
    th.ajout_mot("dans")
    th.ajout_mot("le")
    th.ajout_mot("lourd")
    th.ajout_mot("tapis")
    th.ajout_mot("de")
    th.ajout_mot("luxe")
    th.ajout_mot("vert")
    th.ajout_mot("olive")

    th_exemple = TrieHybride.file_to_trie('../exemple_base.txt')

    #thybrid_to_gviz(th)
    thybrid_to_gviz(th_exemple)
    print(th_exemple.liste_mots())
    print("GVIZ GENERATION DONE")

if __name__ == "__main__":
    main()