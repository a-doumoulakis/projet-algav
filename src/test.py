import cProfile
# pour les calculs de temps d'execution
# https://docs.python.org/2/library/profile.html
import sys
from Briandais import Briandais
from TrieHybride import TrieHybride


if __name__ == "__main__":
    if "test" in sys.argv:
        f_test = 1
    else:
        f_test = 0
    print("Exemple de base : Arbre de la Briandais")
    test = Briandais.file_to_briandais("exemple_base.txt")
    print("Visualisation de l'abre :")
    test.best_print()
    liste = []
    liste = test.liste_mots()
    print("\nliste mots :", liste)
    print("nombre mots :", test.nombre_mots())
    print("nombre de pointeurs vers nil :", test.cmp_none())
    print("hauteur de l'arbre:", test.hauteur_arbre())
    print("profondeur moyenne:", test.profondeur_moyenne())
    print("prefixe 'dactylo' :", test.nombre_prefix("dactylo"))
    # test2 = TrieHybride.briandais_to_hybride(test, None)
    # liste = test2.liste_mots()
    # print(liste)

    test = Briandais.files_to_briandais_ajou("./Shakespeare")
    if f_test is 1:
        print('\033[91m', "\n Construction Arbre par Fusion\n\n", '\033[0m')
        cProfile.run('Briandais.files_to_briandais_fus("./Shakespeare")')

        print('\033[91m', "\n Construction Arbre par Ajout \n\n", '\033[0m')
        cProfile.run('Briandais.files_to_briandais_ajou("./Shakespeare")')

        print('\033[91m', "\n Ajouter barbapapa \n\n", '\033[0m')
        cProfile.run('test.ajouter_mot("barbapapa")')
        
        print('\033[91m', "\n Liste mots \n\n", '\033[0m')
        cProfile.run('test.liste_mots()')

        print('\033[91m', "\n Nombre mots \n\n", '\033[0m')
        cProfile.run('test.nombre_mots()')

    liste = test.liste_mots()
    liste.sort()
    file = open("word_list/briandais.txt", 'w')
    for el in liste:
        file.write(el + '\n')
    file.close()
    print("nombre de mots :", test.nombre_mots())
    print("profondeur moyenne :", test.profondeur_moyenne())
    print("hauteur :", test.hauteur_arbre())
    print("cpt none:", test.cmp_none())
