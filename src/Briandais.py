import os
# import pydotplus
# Pour les graphs


class Briandais:
    """ Classe qui représente les arbre de la Briandais. Un arbre de la Briandais """

    def __init__(self, mot=None):
        """ Constructeur d'un arbre de la Briandais qui construit un arbre à partir d'un mot donné en argument """

        if mot is None:
            self.lettre = '~'
            self.fils = None
            self.frere = None  # Si il n'y a pas de mot alors on crée l'abre vide
        else:
            self.lettre = mot[0]
            # Donne la première lettre du mot
            self.frere = None
            self.fils = Briandais(Briandais.__fin_mot(mot))
            # Appel recusif avec le mot privé de sa première lettre ou None si le mot est vide

    def __str__(self):
        """ Affiche un arbre de la Briandais """

        if (self.lettre is '~') and (self.fils is None):
            print("~")
        else:
            print(self.lettre, end="")

        if self.fils is not None:
            print(self.fils, end="")

        if self.frere is not None:
            print(self.frere, end="")

        return ""

    @staticmethod
    def create_briandais(lettre, frere, fils):
        """ Renvoie un nouvel arbre de la Briandais aillant les arguments de la fonction comme attributs
            :param lettre: La lettre du nouvel arbre
            :param frere: Le frère du nouvel arbre
            :param fils: Le fils du nouvel abre
            :return: L'arbre de la Briandais avec les attributs demandés"""

        res = Briandais()
        res.lettre = lettre
        res.frere = frere
        res.fils = fils
        return res

    def best_print(self, nb=0, de_frere=False):
        """ Meilleur impression des mots d'un arbre de la Briandais
        :param nb: Nombre d'espace avant l'impression
        :param de_frere: Indique si on vient d'un frère ou pas pour savoir si on doit faire l'impression
        :return: None"""

        if self.lettre is '~' and self.fils is None:
            print("~")
        else:
            if de_frere:
                print(" " * nb, self.lettre, end="")
            else:
                print(self.lettre, end="")
        if self.fils is not None:
            self.fils.best_print(nb, False)
        if self.frere is not None:
            self.frere.best_print(nb + 1, True)
        return None

    def est_vide(self):
        """ Renvoie vrai si l'agument est un arbre de la Briandais vide, faux sinon
        :return: La valeur de verité"""

        if (self.lettre is '~') and (self.fils is None) and (self.frere is None):
            return True

        return False

    def liste_mots(self):
        liste = []
        return self.__liste_mots(liste)

    def __liste_mots(self, liste, mot=""):
        """ Renvoie la liste des mots présents dans un arbre de la Briandais
         :param liste: Liste à remplir avec les mots de l'arbre
         :param mot: Chaine qui sert à se rappeler du chemin parcouru dans l'arbre
         :return: La liste remplie avec les mots présent dans l'arbre"""

        nouv_mot = ""
        if self.lettre is '~':
            if mot not in liste:
                liste.append(mot)
        else:
            nouv_mot = mot + self.lettre

        if self.fils is not None:
            liste = self.fils.__liste_mots(liste, nouv_mot)
            # Appel recursif avec le mot auquel on a jouté la nouvelle lettre

        if self.frere is not None:
            liste = self.frere.__liste_mots(liste, mot)
            # Appel au frère avec le mot inchangé

        return liste

    def nombre_mots(self):
        """ Renvoie le nombre de mots dans un arbre de la Briandais 
        :return: Le nombre de mot dans l'arbre"""

        total = 0
        if self.lettre is '~':
            total += 1

        if self.frere is not None:
            total += self.frere.nombre_mots()

        if self.fils is not None:
            total += self.fils.nombre_mots()

        return total

    def nombre_prefix(self, prefix):
        """ Renvoie le nombre de mot de l'arbre de la Briandais dont 'prefix' est le prefixe
        :param prefix: Le mot pour lequel on compte le nombre de mots pour lesquels il est prefixe
        :return: Le nombre de mot qui on 'prefix' comme prefixe"""

        if prefix is None:
            # On est arrivé au point ou tous les mot on le même prefixe
            return self.nombre_mots()

        lettre = '\0' if self.lettre is '~' else self.lettre
        if prefix[0] < lettre:
            return 0

        elif prefix[0] > lettre:
            return self.frere.nombre_prefix(prefix)

        else:
            return self.fils.nombre_prefix(Briandais.__fin_mot(prefix))

    def est_present(self, mot):
        """ Renvoie True si le mot est présent dans l'arbre de la Briandais Faux sinon
        :param mot: Le mot pour lequel on veut vérifier sa présence
        :return: Booléen indiquant si le mot est présent ou pas"""

        if mot is None:
            return self.lettre is '~'

        if self.est_vide():
            return False

        head = mot[0]
        if self.lettre is '~':
            cmpa = '\0'
        else:
            cmpa = self.lettre

        if head < cmpa:
            # Car '~' est supérieur à tous les autre charactère et il devrait être inférieur
            return False

        if head is self.lettre:
            if self.fils is None:
                return False

            return self.fils.est_present(Briandais.__fin_mot(mot))
            # Appel recursif avec None comme mot si il c'est une lettre

        if self.frere is None:
            return False
        return self.frere.est_present(mot)

    def ajouter_mot(self, mot):
        """ Retourne l'arbre de la Briandais auquel on a ajouté le mot passé en argument
         :param mot: Le mot à ajouter
         :return: L'arbre de la Briandais mis à jour """
        # En raison de la mauvaise performance de la construction des arbres de la Briandais, cette fonction à dû être
        # optimisée (en enlevant des appel a la fonction create_briandais), au lieu de créer un nouvel arbre dans tous
        # les cas, cela est fait uniquement quand il le faut sinon la structure est modifiée "sur place", le code
        # est donc moins lisible mais plus rapide
        if self.est_vide():
            # Si la feuille est vide on construit le reste du mot si il existe
            if mot is None:
                return self
            return Briandais(mot)

        if mot is None:
            # On a trouvé ou mettre le mot
            if self.lettre is '~':
                # Le mot existe déjà dans l'arbre
                return self
            # On renvoie un nouvel arbre aillant comme frère l'ancien arbre et comme lettre le caractère de fin
            tmp = Briandais()
            tmp.lettre = '~'
            tmp.frere = self
            tmp.fils = None
            return tmp
            # Briandais.create_briandais('~', self, None)

        head = mot[0]
        # Pour faire en sorte que le caractère de fin de chaine est inférieur à tous les autres
        cmp = '\0' if self.lettre is '~' else self.lettre

        if head < cmp:
            # Il faut placer le nouveau mot avant celui qui existe déjà
            # On renvoie un arbre contenant le mot à ajouter et comme frère les mots existant
            tmp = Briandais()
            tmp.lettre = head
            tmp.frere = self
            tmp.fils = Briandais(None if len(mot) is 1 else mot[1:])
            return tmp

        if head > cmp:
            # Il faut placer le nouveau mot apres celui qui existe déjà
            # On ajoute le mot au frère si il existe sinon on lui donne un nouveau frère contenant le nouveau mot
            if self.frere is None:
                self.frere = Briandais(mot)
                return self
            self.frere = self.frere.ajouter_mot(mot)
            return self
        else:
            # On dois ajouter le mot au fils
            if self.fils is None:
                self.fils = Briandais(None if len(mot) is 1 else mot[1:])
                return self
            else:
                if self.fils.est_vide():
                    if len(mot) is 1:
                        return self
                    self.fils.frere = Briandais(mot[1:])
                    return self
                self.fils = self.fils.ajouter_mot(None if len(mot) is 1 else mot[1:])
                return self

    def supprimer_mot(self, mot):
        """ Supprime un mot d'un arbre de la Briandais
        :param mot: Le mot à supprimer
        :return: Un arbre de la Briandais sans le mot entré en argument """

        if self.est_vide() and mot is None:
            # La case qui contient le mot n'a pas de frères ni de fils
            return None

        if mot is None:
            # Si le mot est vide, pour pouvoir passer les test
            mot = "~"

        if self.lettre is mot[0]:
            if self.fils is not None:
                fils = self.fils.supprimer_mot(Briandais.__fin_mot(mot))
            else:
                fils = None

            if fils is None:
                return self.frere
            else:
                res = Briandais()
                res.lettre = self.lettre
                res.fils = fils
                res.frere = self.frere
                return res

        elif self.lettre > mot[0]:
            return self

        else:
            res = Briandais()
            res.lettre = self.lettre
            res.fils = self.fils
            if res.frere is not None:
                res.frere = self.frere.supprimer_mot(mot)
            return res

    def cmp_none(self):
        """ Compte le nombre de pointeur vers None dans un arbre de la Briandais
        :return: Le nombre de pointeur vers None """
        cpt = 0
        if self.est_vide():
            return 2

        if self.fils is None:
            cpt += 1
        else:
            cpt += self.fils.cmp_none()

        if self.frere is None:
            cpt += 1
        else:
            cpt += self.frere.cmp_none()

        return cpt

    def hauteur_arbre(self):
        """ Calcule la hauteur d'un arbre de la Briandais
        :return: La hauteur de l'arbre """
        hauteur = 0
        if self.est_vide():
            return 0
        if self.fils is not None:
            hauteur = 1 + self.fils.hauteur_arbre()
        if self.frere is not None:
            hauteur = max(1 + self.frere.hauteur_arbre(), hauteur)
        return hauteur

    def profondeur_moyenne(self):
        """ Calcule la profondeur moyenne d'un arbre de la Briandais
        :return: La pronfondeur moyenne de l'arbre, False si la fonction echoue """

        tab = [0, 0]
        # tab[0] = niveau(somme des profondeurs), tab[1] = compte(nombre de feuille)
        niveau_act = 1
        tab = Briandais.__profondeur_moyenne(self, tab, niveau_act)
        return tab[0]/tab[1]

    @staticmethod
    def __profondeur_moyenne(arbre, tab, niveau_act):
        if arbre is None:
            return tab

        if arbre.fils is None and arbre.frere is None:
            tab[0] += niveau_act
            tab[1] += 1
            return tab

        if arbre.fils is not None:
            tab = Briandais.__profondeur_moyenne(arbre.fils, tab, niveau_act+1)

        if arbre.frere is not None:
            tab = Briandais.__profondeur_moyenne(arbre.frere, tab, niveau_act)
            
        return tab

    @staticmethod
    def fusion_arbre(arbre1, arbre2):
        """ Fusionne deux arbres la Briandais passés en argument
        :param arbre1: Le premier arbre que l'on veut fusionner
        :param arbre2: Le deuxième arbre que l'on veut fusionner
        :return: L'arbre de la Briandais qui resulte de la fusion des deux arbres """

        if arbre1 is None:
            return arbre2

        if arbre2 is None:
            return arbre1

        # Permet de contourner le problème de la conparaison de '~'
        # qui est supérieur à tous les caractères alphanumériques
        lettre_a1 = '\0' if arbre1.lettre is '~' else arbre1.lettre
        lettre_a2 = '\0' if arbre2.lettre is '~' else arbre2.lettre

        if lettre_a2 < lettre_a1:
            res = Briandais()
            res.lettre = arbre2.lettre
            res.fils = arbre2.fils
            res.frere = Briandais.fusion_arbre(arbre1, arbre2.frere)
            return res

        elif lettre_a2 > lettre_a1:
            res = Briandais()
            res.lettre = arbre1.lettre
            res.fils = arbre1.fils
            res.frere = Briandais.fusion_arbre(arbre1.frere, arbre2)
            return res

        else:
            res = Briandais()
            res.lettre = arbre1.lettre
            res.fils = Briandais.fusion_arbre(arbre1.fils, arbre2.fils)
            res.frere = Briandais.fusion_arbre(arbre1.frere, arbre2.frere)
            return res

#    @staticmethod
#    def generer_graph(nom_fic, arbre):
#        """ Génère un fichier png contenant le graph de l'abre passé en argument,
#        le fichier porte le nom de l'argument nom
#        :param nom_fic: Nom du fichier qui vas être créé
#        :param arbre: Arbre pour lequel le fichier vas être généré
#        :return: None """
#    graph = pydotplus.Dot(graph_name="Arbre de la Briandais")
#    graph.set_type("Digraph")
#    node1 = pydotplus.Node("A", "test")
#    node2 = pydotplus.Node("B", "test2")
#    edge = pydotplus.Edge("A", "B")
#    graph.add_edge(edge)
#    graph.write("./test.png", format="png")
    # TODO A finir

    @staticmethod
    def __fin_mot(mot):
        # None si le mot est de longueur 1, les n-1 dernières lettres si mot de longueur n > 1
        return None if len(mot) is 1 else mot[1:]
    
    @staticmethod
    def file_to_briandais(filename):
        fic = open(filename)
        data = fic.read().split()
        test = Briandais(data[0])
        for mot in data[1:]:
            test = test.ajouter_mot(mot)
        print("Fin d'ajout :", filename)
        return test
            
    @staticmethod
    def files_to_briandais_fus(directory):
        test = Briandais()
        for filename in os.listdir(directory):
            test = Briandais.fusion_arbre(test, Briandais.file_to_briandais(directory + "/" + filename))
        return test

    @staticmethod
    def files_to_briandais_ajou(directory):
        test = Briandais("the")
        for filename in os.listdir(directory):
            fic = open(directory + "/" + filename)
            data = fic.read().split()
            for mot in data:
                test = test.ajouter_mot(mot)
            print("Fin d'ajout : ", filename)
        return test
