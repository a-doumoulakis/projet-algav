from Briandais import *
from visualTHybrid import *
import os


class TrieHybride:

    def __init__(self, word=None):
        if word is None:
            # boolean indicating the end of a word within the trie
            self.isWord = True
            self.val = "\0"

            self.inf = None
            self.eq = None
            self.sup = None
        else:
            self.val = word[0]
            self.inf = None
            self.sup = None
            if len(word) is 1:
                self.isWord = True
                self.eq = None
            else:
                self.isWord = False
                self.eq = TrieHybride(word[1:])

    def __str__(self):
        ht = self.hauteur_trie()
        print(ht)
        if not self.est_vide() and self.val is not None:
            s = ht * " " + str(self.val)
            return s+'\n' + str(self.inf) + str(self.eq) + str(self.sup)
        else:
            return ''

    @staticmethod
    def create_thybrid(val, inf, eq, sup, flag=False):
        new_trie = TrieHybride()

        new_trie.val = val
        new_trie.isWord = flag

        new_trie.inf = inf
        new_trie.eq = eq
        new_trie.sup = sup
        return new_trie

    def est_vide(self):
        if self.val is "\0":
            return True
        return False

    def val(self):
        return self.val

    def est_fin_de_mot(self):
        return self.isWord

    def sub_tree_inf(self):
        return self.inf

    def sous_arbre_eq(self):
        return self.eq

    def sous_arbre_sup(self):
        return self.sup

    def ajout_mot(self, mot):
        # Ajoute un mot au trie hybride

        next_w = mot[0]
        if self.val is '\0':
            self.isWord = False
            self.val = next_w
        if next_w < self.val:
            if self.inf is None:
                self.inf = TrieHybride(mot)
            else:
                self.inf = self.inf.ajout_mot(mot)

        elif next_w > self.val:
            if self.sup is None:
                self.sup = TrieHybride(mot)
            else:
                self.sup = self.sup.ajout_mot(mot)
        else:

            if len(mot) is 1:
                self.isWord = True
            else:
                if self.eq is None:
                    self.eq = TrieHybride(mot[1:])
                else:
                    self.eq = self.eq.ajout_mot(mot[1:])
        return self

    def ajout_branche(self, branche):
        lettre = branche.val
        print("lettre " + str(lettre))
        if self.est_vide():
            return branche
        elif lettre < self.val:
            if self.inf is None:
                self.inf = branche
            else:
                self.inf = self.inf.ajout_branche(branche)
            return self
        elif lettre > self.val:
            if self.sup is None:
                self.sup = branche
            else:
                self.sup = self.sup.ajout_branche(branche)
            return self
        else:
            print("problème sérieux (ajout_branche)")
            # ce cas ne devrait pas se produire dans un ajout apres une suppression

    def supprimer_mot(self, mot):
        if len(mot) is 1:
            if self.isWord is True:  # le mot finit sur ce noeud
                if self.eq is not None:  # si le mot a supprimer est un prefixe d'un autre mot
                    self.isWord = False
                elif self.inf is not None:
                    self.isWord = False
                    self.eq = self.inf
                    self.inf = None
                elif self.sup is not None:
                    self.isWord = False
                    self.eq = self.sup
                    self.sup = None
                else:
                    return None
            return self
        else:
            if mot[0] < self.val:
                if self.inf is not None:
                    self.inf = self.inf.supprimer_mot(mot)
            elif mot[0] > self.val:
                if self.sup is not None:
                    self.sup = self.sup.supprimer_mot(mot)
            else:
                if self.eq is not None:
                    self.eq = self.eq.supprimer_mot(mot[1:])

            if self.inf is None and self.eq is None and self.sup is None:
                return None

            elif self.eq is None and self.inf is not None:
                if self.sup is None:
                    return self.inf
                return self.inf.ajout_branche(self.sup)

            elif self.eq is None and self.sup is not None:
                # self = self.create_thybrid(self.sup.val, self.inf, self.sup, None, self.sup.isWord)
                if self.inf is None:
                    return self.sup
                return self.sup.ajout_branche(self.inf)

            return self

    def est_present(self, mot):
        # True si le mot est dans l'arbre, False sinon
        if self.est_vide() or mot is None:
            return False

        if head(mot) < self.val:
            if self.inf is None:
                return False
            else:
                self.inf.est_present(None if len(mot) is 1 else tail(mot))

        if head(mot) > self.val:
            if self.sup is None:
                return False
            else:
                return self.sup.est_present(None if len(mot) is 1 else tail(mot))
        else:
            if len(mot) is 1:
                return self.isWord
            if self.eq is None:
                return False
            else:
                return self.eq.est_present(None if len(mot) is 1 else tail(mot))

    def cpt_mot(self):
        # Retourne le nombre de mot dans un trie hybride
        inf = 0
        eq = 0
        sup = 0
        if self.inf is not None:
            inf = self.inf.cpt_mot()
        if self.eq is not None:
            eq = self.eq.cpt_mot()
        if self.sup is not None:
            sup = self.sup.cpt_mot()
        if self.isWord:
            return 1 + inf + eq + sup
        else:
            return inf + eq + sup

    def liste_mots_rec(self, liste, mot=""):
        # Renvoie une liste avec les mots contenu dans un trie hybride
        new_mot = mot + self.val
        if self.isWord:
            liste.append(new_mot)
        if self.inf is not None:
            liste = self.inf.liste_mots_rec(liste, mot)
        if self.eq is not None:
            liste = self.eq.liste_mots_rec(liste, new_mot)
        if self.sup is not None:
            liste = self.sup.liste_mots_rec(liste, mot)
        return liste

    def liste_mots(self):
        liste = []
        return self.liste_mots_rec(liste)

    def cpt_none(self):
        # Compte le nombre de pointeurs vers None
        tot = 0
        if self.eq is None:
            tot += 1
        else:
            tot += self.eq.cpt_none()
        if self.inf is None:
            tot += 1
        else:
            tot += self.inf.cpt_none()
        if self.sup is None:
            tot += 1
        else:
            tot += self.sup.cpt_none()
        return tot

    def hauteur_trie(self):
        # Renvoie la hauteur de l'arbre
        inf = 0
        eq = 0
        sup = 0
        if self.inf is not None:
            inf = self.inf.hauteur_trie()
        if self.eq is not None:
            eq = self.eq.hauteur_trie()
        if self.sup is not None:
            sup = self.sup.hauteur_trie()
        return 1 + max(inf, eq, sup)

    def nombre_prefix(self, prefix):
        if len(prefix) is 1:
            return self.cpt_mot()
        if prefix[0] < self.val:
            if self.inf is None:
                return 0
            else:
                return self.inf.nombre_prefix(prefix)
        if prefix[0] > self.val:
            if self.sup is None:
                return 0
            else:
                return self.sup.nombre_prefix(prefix)
        if self.eq is None:
            return 0
        else:
            return self.eq.nombre_prefix(prefix[1:])

    def profondeur_moyenne(self):
        tab = [0, 0]
        # tab[0] = niveau(somme des profondeurs), tab[1] = compte(nombre de feuille)
        niveau_act = 1
        tab = TrieHybride.__profondeur_moyenne(self, tab, niveau_act)
        return tab[0]/tab[1]

    @staticmethod
    def __profondeur_moyenne(trie, tab, niveau_act):
        if trie is None:
            return tab
        if trie.inf is None and trie.eq is None and trie.sup is None:
            tab[0] += niveau_act
            tab[1] += 1
            return tab
        if trie.inf is not None:
            tab = TrieHybride.__profondeur_moyenne(trie.inf, tab, niveau_act+1)
        if trie.eq is not None:
            tab = TrieHybride.__profondeur_moyenne(trie.eq, tab, niveau_act+1)
        if trie.sup is not None:
            tab = TrieHybride.__profondeur_moyenne(trie.sup, tab, niveau_act+1)
        return tab

    @staticmethod
    def thybrid_to_briandais_pour_les_nuls(trie, arbre, mot=""):
        if trie is None:
            return arbre
        nouv = mot + trie.val
        if trie.isWord:
            arbre.ajouter_mot(nouv)
        arbre = TrieHybride.thybrid_to_briandais_pour_les_nuls(trie.eq, arbre, nouv)
        arbre = TrieHybride.thybrid_to_briandais_pour_les_nuls(trie.inf, arbre, mot)
        arbre = TrieHybride.thybrid_to_briandais_pour_les_nuls(trie.sup, arbre, mot)

        return arbre

    @staticmethod
    def briandais_to_hybride(arbre, thyb):
        if thyb is None:
            thyb = TrieHybride()
            thyb.isWord = False
        if arbre is None:
            return thyb
        next_l = arbre.lettre
        if next_l is not '~':
            if thyb.val is '\0':
                thyb.val = next_l
                if arbre.fils is not None and arbre.fils.lettre is '~':
                    thyb.isWord = True
                thyb.eq = TrieHybride.briandais_to_hybride(arbre.fils, thyb.eq)
            elif thyb.val is next_l:
                if arbre.fils is not None and arbre.fils.lettre is '~':
                    thyb.isWord = True
                thyb.eq = TrieHybride.briandais_to_hybride(arbre.fils, thyb.eq)
            
            elif next_l < thyb.val:
                thyb.inf = TrieHybride.briandais_to_hybride(arbre, thyb.inf)

            elif next_l > thyb.val:
                thyb.sup = TrieHybride.briandais_to_hybride(arbre, thyb.sup)
            thyb = TrieHybride.briandais_to_hybride(arbre.frere, thyb)    
        return thyb

    @staticmethod
    def file_to_trie(filename):
        test = TrieHybride()
        fic = open(filename)
        data = fic.read().split()
        for mot in data:
            test = test.ajout_mot(mot)
        print("Fin d'ajout : ", filename)
        return test

    @staticmethod
    def dir_to_trie(directory):
        test = TrieHybride()
        for filename in os.listdir(directory):
            fic = open(directory + "/" + filename)
            data = fic.read().split()
            for mot in data:
                test = test.ajout_mot(mot)
            print("Fin d'ajout : ", filename)
        return test

    def thybrid_to_briandais(self):
        if self.est_vide():
            return None

        brd = Briandais()
        TrieHybride.thybrid_to_briandais_rec(self, brd)
        return brd

    @staticmethod
    def thybrid_to_briandais_rec(th, brd, remonte=False):
        print("REMONTE : +" + str(remonte))
        # on parcourt les fils gauche du trie pour trouver la plus petite première lettre
        if th.inf is not None and remonte is False:
            TrieHybride.thybrid_to_briandais_rec(th.inf, brd)

        # construction du briandais a partir du plus petit prefixe
        print("traitement de :" + str(th.val))

        if brd.lettre is '~':
            brd.lettre = th.val
            if th.isWord is True:
                print("THIS ISWORD")
                brd.fils = Briandais()  # on insere un fils de lettre '~' pour marquer la fin de mot
                next_is_frere = True   # un briandais avec '~' n'a pas de fils : poursuite sur le frere
            else:
                next_is_frere = False  # sinon on peut continuer normalement sur le fils

            if th.eq is not None:
                if next_is_frere is True:
                    brd.frere = Briandais()
                    TrieHybride.thybrid_to_briandais_rec(th.eq, brd.frere)
                else:
                    brd.fils = Briandais()
                    TrieHybride.thybrid_to_briandais_rec(th.eq, brd.fils)
                    # TrieHybride.thybrid_to_briandais(th.eq, tmp_fils)

            if th.sup is not None:
                brd.frere = Briandais()
                TrieHybride.thybrid_to_briandais_rec(th.sup, brd.frere)
        else:
            brd.frere = Briandais()
            TrieHybride.thybrid_to_briandais_rec(th, brd.frere, True)

    # methode de rotation
    def rotationG(self):
        b = self.sup
        c = TrieHybride.create_thybrid(self.val, self.inf, self.eq, b.inf, self.isWord)
        return TrieHybride.create_thybrid(b.val, c, self.sup.eq, b.sup, b.isWord)

    def rotationD(self):
        b = self.inf
        c = TrieHybride.create_thybrid(self.val, b.sup, self.eq, self.sup, self.isWord)
        return TrieHybride.create_thybrid(b.val, b.inf, b.eq, c, b.isWord)

    # calcul de la hauteur qui ne prend pas en compte le sous arbre eq
    def hauteur_equilibrage(self):
        # Renvoie la hauteur de l'arbre
        inf = 0
        sup = 0
        if self.inf is not None:
            inf = self.inf.hauteur_equilibrage()
        if self.sup is not None:
            sup = self.sup.hauteur_equilibrage()
        return 1 + max(inf, sup)

    # equilibrage à exécuter après insertion
    def equilibrage(self):

        if self.inf is None:
            hinf = 0
        else:
            hinf = self.inf.hauteur_equilibrage()
        if self.sup is None:
            hsup = 0
        else:
            hsup = self.sup.hauteur_equilibrage()

        print("hinf : " + str(hinf) + " hsup :"+ str(hsup))
        if hinf - hsup == 2:

            # lol on aurait du faire du fonctionel yo
            if self.inf.inf is None:
                hinfinf = 0
            else:
                hinfinf = self.inf.inf.hauteur_equilibrage()
            if self.inf.sup is None:
                hinfsup = 0
            else:
                hinfsup = self.sup.sup.hauteur_equilibrage()
            # ********************************************* #

            if hinfinf < hinfsup:
                self.inf = self.inf.rotationD()
                self = self.rotationG()
                #return self
            else:
                self = self.rotationD()
                #return self
        if hinf - hsup == -2: # arbre droit trop profond

            # lol trop de tests
            if self.sup.inf is None:
                hsupinf = 0
            else:
                hsupinf = self.sup.inf.hauteur_equilibrage()
            if self.sup.sup is None:
                hsupsup = 0
            else:
                hsupsup = self.sup.sup.hauteur_equilibrage()
            # ********************************************* #

            print("-2 .. hsupinf: "+ str(hsupinf)+ " hsupsup:" + str(hsupsup))
            if hsupinf < hsupsup:
                self = self.rotationG()
                #return self
            else:
                self.sup = self.sup.rotationD()
                self = self.rotationG()
                #return self

        return self

    def insertion_equilibre(self, mot):
        self = self.ajout_mot(mot)
        self.equilibrage()
        return self


def head(x):
    return x[0]


def tail(x):
    return x[1:]


def main_test():
    print("FONCTION main_test")

    th = TrieHybride.file_to_trie("../exemple_base.txt")
    thybrid_to_gviz(shakespeareth, 'thybrid_exemplebase')

    brd = Briandais.file_to_briandais("../exemple_base.txt")
    briandais_to_gviz(brd, "brd_exemplebase")

if __name__ == " __main__":
    main_test()
